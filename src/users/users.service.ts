import { Injectable, UnauthorizedException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './entities/user.entity';
import { PaginateModel, Types } from 'mongoose';
import { FilterUserDto } from './dto/filter-user.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name)
    private userModel: PaginateModel<User>,
  ) {}

  create(createUserDto: CreateUserDto) {
    return 'This action adds a new user';
  }

  async findAll(filterUserDto: FilterUserDto) {
    const result = await this.userModel.paginate(
      {},
      { page: filterUserDto.page, limit: filterUserDto.limit },
    );
    return {
      docs: result.docs,
      totalDocs: result.totalDocs,
      totalPages: result.totalPages,
    };
  }

  findOne(id: string) {
    const user = this.userModel
      .findById(Types.ObjectId.createFromHexString(id))
      .exec();
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    return `This action updates a #${id} user`;
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }
}

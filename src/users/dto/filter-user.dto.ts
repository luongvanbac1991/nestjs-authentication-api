import { FilterDto } from '../../common/filter.dto';
import { IsOptional } from 'class-validator';

export class FilterUserDto extends FilterDto {
  @IsOptional()
  search: string = '';
}

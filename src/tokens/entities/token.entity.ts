import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Types } from 'mongoose';
import { User } from '../../users/entities/user.entity';

@Schema({ collection: 'tokens', autoCreate: true, timestamps: true })
export class Token {
  @Prop({ required: true, type: Types.ObjectId, ref: 'User' })
  user: User;
}

export const TokenSchema = SchemaFactory.createForClass(Token);

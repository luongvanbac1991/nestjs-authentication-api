import { Controller, Param, Delete } from '@nestjs/common';
import { TokensService } from './tokens.service';
import { Roles } from '../common/roles.decorator';
import { Role } from '../common/role.enum';

@Controller('tokens')
export class TokensController {
  constructor(private readonly tokensService: TokensService) {}

  @Delete(':id')
  @Roles(Role.Admin)
  remove(@Param('id') id: string) {
    return this.tokensService.remove(+id);
  }
}

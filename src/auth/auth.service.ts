import {
  BadRequestException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { LoginDto } from './dto/login.dto';
import { RegisterDto } from './dto/register.dto';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User } from '../users/entities/user.entity';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { Token } from '../tokens/entities/token.entity';
import { EmailsService } from '../emails/emails.service';
import { Otp, OtpType } from '../otps/entities/otp.entity';
import { ForgotPasswordDto } from './dto/forgot-password.dto';
import { ResetPasswordDto } from './dto/reset-password';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(User.name)
    private userModel: Model<User>,
    @InjectModel(Token.name)
    private tokenModel: Model<Token>,
    @InjectModel(Otp.name)
    private otpModel: Model<Otp>,
    private jwtService: JwtService,
    private emailsService: EmailsService,
  ) {}

  async login(loginDto: LoginDto) {
    const user = await this.userModel.findOne({ email: loginDto.email });
    if (
      !user ||
      !bcrypt.compareSync(loginDto.password, user.password) ||
      user.active == false
    ) {
      throw new UnauthorizedException('Email or password incorrect');
    }
    const userId = user._id.toString();
    const savedToken = await this.tokenModel.create({ user: user });
    const payload = {
      sub: savedToken._id,
      id: userId,
    };
    const accessToken = this.jwtService.sign(payload);
    return {
      accessToken: accessToken,
      user: user,
    };
  }

  async register(registerDto: RegisterDto) {
    const existingUser = await this.userModel.findOne({
      email: registerDto.email,
    });
    if (existingUser && existingUser.active == true) {
      throw new BadRequestException('User already exists');
    }
    registerDto.password = bcrypt.hashSync(registerDto.password, 12);
    let savedUser: User;
    if (existingUser && existingUser.active == false) {
      await this.userModel.findByIdAndUpdate(existingUser._id, registerDto);
      savedUser = await this.userModel.findOne({ _id: existingUser._id });
    } else {
      savedUser = await this.userModel.create(registerDto);
    }
    const savedOpt = await this.otpModel.create({
      user: savedUser,
      type: OtpType.ConfirmEmail,
      expiryTime: new Date(Date.now() + 900000),
    });
    await this.emailsService.sendEmailConfirmation(
      savedUser,
      savedOpt._id.toString(),
    );
    return savedUser;
  }

  async confirmEmail(otp: string) {
    const existingOtp = await this.otpModel.findOne({ _id: otp });
    if (
      !existingOtp ||
      existingOtp.type != OtpType.ConfirmEmail ||
      existingOtp.expiryTime.getTime() < Date.now()
    ) {
      throw new BadRequestException();
    }
    await this.otpModel.deleteOne({ _id: existingOtp._id });
    await this.userModel.findByIdAndUpdate(existingOtp.user.toString(), {
      active: true,
    });
    const savedUser = this.userModel.findOne({
      _id: existingOtp.user.toString(),
    });
    if (!savedUser) {
      throw new BadRequestException();
    }
    return savedUser;
  }

  async logout(request: any) {
    const tokenId = request.user.sub;
    await this.tokenModel.deleteOne({ _id: tokenId });
    return 'Logged out';
  }

  async logoutAll(request: any) {
    const userId = request.user.id;
    await this.tokenModel.deleteMany({
      user: Types.ObjectId.createFromHexString(userId),
    });
    return 'Logged out';
  }

  async getProfile(request: any) {
    const id = request.user.id;
    const user = this.userModel.findOne({ _id: id });
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }

  async forgotPassword(forgotPasswordDto: ForgotPasswordDto) {
    const existingUser = await this.userModel.findOne({
      email: forgotPasswordDto.email,
    });
    if (existingUser) {
      const savedOpt = await this.otpModel.create({
        user: existingUser,
        type: OtpType.ResetPassword,
        expiryTime: new Date(Date.now() + 900000),
      });
      await this.emailsService.sendEmailResetPassword(
        existingUser,
        savedOpt._id.toString(),
      );
    }
    return 'If the account exists, we will send you an email to reset your password';
  }

  async resetPassword(resetPasswordDto: ResetPasswordDto) {
    const existingOtp = await this.otpModel.findOne({
      _id: resetPasswordDto.otp,
    });
    if (
      !existingOtp ||
      existingOtp.type != OtpType.ResetPassword ||
      existingOtp.expiryTime.getTime() < Date.now()
    ) {
      throw new BadRequestException();
    }
    await this.otpModel.deleteOne({ _id: existingOtp._id });
    const savedUser = await this.userModel.findByIdAndUpdate(
      existingOtp.user.toString(),
      { password: bcrypt.hashSync(resetPasswordDto.password, 12) },
    );
    if (!savedUser) {
      throw new NotFoundException('User not found');
    }
    return savedUser;
  }
}

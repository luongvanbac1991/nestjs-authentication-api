import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from '../users/entities/user.entity';
import { JwtModule } from '@nestjs/jwt';
import { Token, TokenSchema } from '../tokens/entities/token.entity';
import * as process from 'node:process';
import { ConfigModule } from '@nestjs/config';
import { EmailsModule } from '../emails/emails.module';
import { Otp, OtpSchema } from '../otps/entities/otp.entity';

@Module({
  imports: [
    ConfigModule.forRoot({}),
    MongooseModule.forFeature([
      { name: User.name, schema: UserSchema },
      { name: Token.name, schema: TokenSchema },
      { name: Otp.name, schema: OtpSchema },
    ]),
    JwtModule.register({
      global: true,
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: process.env.JWT_EXPIRE },
    }),
    EmailsModule,
  ],
  controllers: [AuthController],
  providers: [AuthService],
})
export class AuthModule {}

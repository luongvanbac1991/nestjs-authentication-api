import { Body, Controller, Get, Param, Post, Request } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { RegisterDto } from './dto/register.dto';
import { Roles } from '../common/roles.decorator';
import { Role } from '../common/role.enum';
import { ForgotPasswordDto } from './dto/forgot-password.dto';
import { ResetPasswordDto } from './dto/reset-password';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  login(@Body() loginDto: LoginDto) {
    return this.authService.login(loginDto);
  }

  @Post('register')
  register(@Body() registerDto: RegisterDto) {
    return this.authService.register(registerDto);
  }

  @Get('confirm-email/:otp')
  confirmPassword(@Param('otp') otp: string) {
    return this.authService.confirmEmail(otp);
  }

  @Roles(Role.Admin, Role.User)
  @Post('logout')
  logout(@Request() request: any) {
    return this.authService.logout(request);
  }

  @Roles(Role.Admin, Role.User)
  @Post('logout-all')
  logoutAll(@Request() request: any) {
    return this.authService.logoutAll(request);
  }

  @Roles(Role.Admin, Role.User)
  @Get('profile')
  profile(@Request() request: any) {
    return this.authService.getProfile(request);
  }

  @Post('forgot-password')
  forgotPassword(@Body() forgotPasswordDto: ForgotPasswordDto) {
    return this.authService.forgotPassword(forgotPasswordDto);
  }

  @Post('reset-password')
  resetPassword(@Body() resetPasswordDto: ResetPasswordDto) {
    return this.authService.resetPassword(resetPasswordDto);
  }
}

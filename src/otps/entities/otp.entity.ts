import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { User } from '../../users/entities/user.entity';
import { Types } from 'mongoose';

@Schema({ collection: 'otps', autoCreate: true })
export class Otp {
  @Prop({ required: true, type: Types.ObjectId, ref: 'User' })
  user: User;

  @Prop({ type: Number, required: true })
  type: OtpType;

  @Prop({ type: Date, required: true })
  expiryTime: Date;
}

export enum OtpType {
  ConfirmEmail = 1,
  ResetPassword = 2,
}

export const OtpSchema = SchemaFactory.createForClass(Otp);

import { Controller, Param, Delete } from '@nestjs/common';
import { OtpsService } from './otps.service';

@Controller('otps')
export class OtpsController {
  constructor(private readonly otpsService: OtpsService) {}

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.otpsService.remove(+id);
  }
}

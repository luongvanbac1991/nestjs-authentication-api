import { Injectable } from '@nestjs/common';
import { CreateEmailDto } from './dto/create-email.dto';
import { UpdateEmailDto } from './dto/update-email.dto';
import { MailerService } from '@nestjs-modules/mailer';
import { User } from '../users/entities/user.entity';
import * as process from 'node:process';

@Injectable()
export class EmailsService {
  constructor(private mailerService: MailerService) {}

  create(createEmailDto: CreateEmailDto) {
    return 'This action adds a new email';
  }

  findAll() {
    return `This action returns all emails`;
  }

  findOne(id: number) {
    return `This action returns a #${id} email`;
  }

  update(id: number, updateEmailDto: UpdateEmailDto) {
    return `This action updates a #${id} email`;
  }

  remove(id: number) {
    return `This action removes a #${id} email`;
  }

  async sendEmailConfirmation(user: User, otp: string) {
    const url = `${process.env.BACKEND_URL}/auth/confirm-email/${otp}`;
    await this.mailerService
      .sendMail({
        to: user.email,
        subject: 'Welcome to Our App! Confirm your Email',
        template: './email-confirmation',
        context: {
          name: user.fullName,
          url,
        },
      })
      .then()
      .catch((error) => console.log(error));
  }

  async sendEmailResetPassword(user: User, otp: string) {
    const url = `${process.env.BACKEND_URL}/auth/reset-password/${otp}`;
    await this.mailerService
      .sendMail({
        to: user.email,
        subject: 'Reset your password',
        template: './email-confirmation',
        context: {
          name: user.fullName,
          url,
        },
      })
      .then()
      .catch((error) => console.log(error));
  }
}

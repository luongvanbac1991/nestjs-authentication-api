import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';
import { Request, Response } from 'express';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    let status: number;
    try {
      status = exception.getStatus();
    } catch {
      status = 500;
    }
    response.status(status).json({
      statusCode: status,
      error: exception.response
        ? exception.response.message
        : exception.message,
      path: request.url,
      timestamp: new Date().toTimeString(),
    });
  }
}

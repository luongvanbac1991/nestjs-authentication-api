import { IsOptional } from 'class-validator';

export class FilterDto {
  @IsOptional()
  page: number = 1;

  @IsOptional()
  limit: number = 20;
}
